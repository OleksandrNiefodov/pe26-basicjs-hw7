/*
Завдання
HW7 - String, date and time
Теоретичні питання
1. Як можна створити рядок у JavaScript?

-можна створити такими основними способами:
 - вказати вид змінної, назву змінної та присвоїти в змінну рядок і обгорнути текст рядка в лапки (Одинарні '', подвійні "", зворотні ``),
 - або використати функцію String() - перетворює значення у рядок. 

2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?

- особливої різниці між одинарними та подвійними лапками немає. Наприклад: у випадку з одинарними лапками, якщо рядках використовуються мови в яких
є апостроф, то тоді сам апсотроф треба буде екранувати слешом.
- у випадку із зворотніми лапками, то всередину цих лапок, а саме в рядок можна вставляти вирази, використовуючи вираз - ${}.

3. Як перевірити, чи два рядки рівні між собою?

- найкращий спосіб, це використати метод localeCompare()

const stringOne = 'Hello there';
const stringTwo = 'Hello there';

console.log(stringOne.localeCompare(stringTwo)); покаже нуль, бо локал компер видає 0 якщо рядки рівні
якщо stringOne більше ніж stringTwo, то консоль видасть 1, а якщо навпаки, то -1

- або можна повернути булеве значення

const stringOne = 'Hello there';
const stringTwo = 'Hello there';

const equality = stringOne.localeCompare(stringTwo) === 0; в цьому випадку повернеться true
console.log(stringOne.localeCompare(stringTwo));

4. Що повертає Date.now()?

- повертає поточний час в значенні timestamp - просто число.

5. Чим відрізняється Date.now() від new Date()?

- основна відмінність полягає в тому, що Date.now() повертає число поточного часу, а new Date() об'єкт з поточним часом.
*/

/*
Практичні завдання
1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.
*/

const isPalindrome = function (palindrome) {
    const text = palindrome.split('');

    for (const letter of text) {
        if (letter !== text[text.length - 1 - text.indexOf(letter)]) {
            return false;
        }
    }
    return true;
}
console.log(`Word is palindrome, right?:`, isPalindrome('adada'));


/*
2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, 
якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false
*/

const isLengthMax = function(text, length) {

    if (text.length <= length) {
        return true;
    }
    return false;
}

console.log(`length check:`, isLengthMax('rocod', 10));

/*
3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. Функція повина повертати 
значення повних років на дату виклику функцію.
*/

const fullUserAge = function () {

    const birthDate = new Date(prompt('Enter your date of birth in YYYY-MM-DD format:'));
    const currentDate = new Date();

    const birthYear = birthDate.getFullYear();
    const birthMonth = birthDate.getMonth() + 1;
    const birthDay = birthDate.getDate();

    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1;
    const currentDay = currentDate.getDate();

    let fullAge = currentYear - birthYear;
    if (currentMonth < birthMonth) {
        fullAge--;
    } 
    else if (currentMonth === birthMonth && currentDay < birthDay) {
        fullAge--;
    }
    return fullAge;
}
// ще було б круто додати перевірки на те що юзе все вірно вніс
console.log(`Full age is:`, fullUserAge());



